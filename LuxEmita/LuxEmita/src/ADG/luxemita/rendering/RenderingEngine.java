package ADG.luxemita.rendering;



import ADG.luxemita.LuxEmita;
import ADG.luxemita.rendering.texture.EasyTexture;
import ADG.luxemita.rendering.texture.TextureEngine;

import static org.lwjgl.opengl.GL11.*;
import  org.lwjgl.opengl.*;
import  org.lwjgl.*;

public class RenderingEngine {
		/**This is the instance by which you access the rendering engine. INVOKE init() BEFORE USE**/
        public static RenderingEngine renderingEngine;
        /**This is the instance by which you access the texture Engine.INVOKE init() BEFORE USE**/
        public static TextureEngine textureEngine;
        /**This is the instance by which you access the Easy Texture Engine.INVOKE init() BEFORE USE**/
        public static EasyTexture easyTexture;

        /** Invoke this method before using the rendering engine or else the rendering engine instance will be Null. ONLY INVOKE ONCE!**/
        public static void init()
        {
                renderingEngine = new RenderingEngine();
        }
        public RenderingEngine()
        {
                LuxEmita.luxLogger.logInfo("Rendering Engine instance made");
                LuxEmita.luxLogger.logInfo("Creating  Texture Engine Instance");
                textureEngine = new TextureEngine();
                LuxEmita.luxLogger.logInfo("Creating Easy Texture Engine Instance");
                easyTexture = new EasyTexture();
                
        }
        /** Before using LWJGL Invoke this method or else LWJGL will have no native path set which will result in an error.**/
        public boolean setUpLWJGL()
        {
                 
                String OS;
                if (LuxEmita.OSRunning.toLowerCase().contains("windows")) 
                	OS = "windows";
                else if (LuxEmita.OSRunning.toLowerCase().contains("mac")) 
                	OS = "macosx";
                else if (LuxEmita.OSRunning.toLowerCase().contains("linux")) 
                	OS = "linux";
                else 
                	OS = "Error";
                
                if (OS == "Error")
                {
                	LuxEmita.luxLogger.logInfo("Sorry. It apears that you are running an Unsuported operating system");
                	return false;
                }
                
                LuxEmita.luxLogger.logInfo("Setting property \"org.lwjgl.librarypath\" to \""+System.getProperty("user.dir") + "\\LuxEmita_lib\\native\\" + OS);
                System.setProperty("org.lwjgl.librarypath",System.getProperty("user.dir") + "\\LuxEmita_lib\\native\\" + OS);
                
                return true;
        }
        /**Invoke this method to create the display for the game.**/
        public void createDisplay()
        {
        	try {
				Display.setDisplayMode(new DisplayMode(640,480));
				Display.setTitle("LuxEmita");
				Display.create();
			} catch (LWJGLException e) {
				// TODO Auto-generated catch block
				LuxEmita.luxLogger.logInfo(e.getMessage());
				e.printStackTrace();
			}
        	LuxEmita.luxLogger.logInfo("Display Created.");
        }
        public void setUpOpenGL()
        {
        	glMatrixMode(GL_PROJECTION);
        	glLoadIdentity();
        	glOrtho(0,640,480,0,1,-1);
        	glMatrixMode(GL_MODELVIEW);
        }
}