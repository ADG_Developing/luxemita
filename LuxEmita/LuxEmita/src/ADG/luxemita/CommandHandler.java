package ADG.luxemita;

import java.util.ArrayList;
import java.util.List;

/*import test.serverHostTest;
import test.serverTest;*/
import ADG.luxemita.rendering.RenderingEngine;

public class CommandHandler {
	public static List<String> commandQue = new ArrayList<String>();

	public static void send(String cmd) throws Exception {
		LuxEmita.luxLogger.logInfo("[Console Input] " + cmd);
		String[] args = cmd.split(" ");
		if(args[0].startsWith("\\"))
		{
			commandQue.add(cmd);
		}
		
		if(args[0].startsWith("/") && args[0].endsWith("exit")){
			System.out.println("Closing Game....");
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			System.exit(0);
		}
		
		
		if(args[0].startsWith("/") && args[0].endsWith("RenderingEngine.easyTexture.getBindingIntForURL")){			
			LuxEmita.luxLogger.logInfo("Image \"" + args[1] + "\" has been binded to the interger " + RenderingEngine.easyTexture.getBindingIntForURL(args[1]));		
			
		}
		/*
		if(args[0].startsWith("/") && args[0].equals("/server") && args[1].equals("create") && !args[2].isEmpty()){			
			serverHostTest serverCreate = new serverHostTest(args);	
		}
		
		if(args[0].startsWith("/") && args[0].equals("/server") && args[1].equals("connect") && !args[2].isEmpty() && !args[3].isEmpty()){
			serverTest serverConnect = new serverTest(args);		
		}
		
		if(args[0].startsWith("/") && args[0].equals("/server") && args[1].equals("say") && !args[2].isEmpty()){
			serverHostTest.say(args);		
		}*/
	}
	
	public static void popCommandCue()
	{
		if(!commandQue.isEmpty())
		{
			String cmd = commandQue.get(0);
			commandQue.remove(0);
			String[] args = cmd.split(" ");
			if(args[0].startsWith("\\") && args[0].endsWith("exit")){
				System.out.println("Closing Game....");
			
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			
				System.exit(0);
			}
		
		
			if(args[0].startsWith("\\") && args[0].endsWith("RenderingEngine.easyTexture.getBindingIntForURL")){			
				LuxEmita.luxLogger.logInfo("Image \"" + args[1] + "\" has been binded to the interger " + RenderingEngine.easyTexture.getBindingIntForURL(args[1]));		
			}
			
			if(args[0].startsWith("\\") && args[0].endsWith("drawViaInt")){			
				//LuxEmita.luxLogger.logInfo("Drawing image bound to " + args[1] + " at X:" + args[2] + " and Y:" + args[3] + " with the dimensions "+ args[4] + " , " + args[5]);		
				RenderingEngine.textureEngine.drawTextureSimple(Integer.parseInt(args[1]),  Double.parseDouble(args[2]),  Double.parseDouble(args[3]),  Double.parseDouble(args[4]),  Double.parseDouble(args[5]));
				commandQue.add(cmd);
			}
		
		}
	}
}
