package ADG.luxemita.blocks;
import ADG.luxemita.LuxEmita;


public class Block {

	public static Block[] blockArray = new Block[64500];
	public static String[] textureArray = new String[64500];
	
	public boolean isMirror;
	public boolean isLightEmitter;
	public boolean isGeneral;
	public boolean isAir;
	public boolean isLightReceiver;
	
	public static Block mirror;
	public static Block mirror2;
	
	private static LuxEmita gameInstance;
	
	public String textureName;
	public int blockID;
	
	public Block(int blockID){
		if(blockArray[blockID] != null)
		{			
			throw new IllegalArgumentException("ID " + blockID + " is already occupide by " + blockArray[blockID] + " when adding" + this);		
		}
		else
		{
			
		}
		
		blockArray[blockID] = this;
	}
	
	@SuppressWarnings("static-access")
	public static void initBlocks(){
		mirror = new Block(1).isMirror(); 
		mirror2 = new Block(2).isMirror();
		
		if(blockArray.length != textureArray.length)
		{
			throw new IllegalArgumentException("Illegal Array Length, Block Array & Texture Array don't match!");
		}else{
			gameInstance.luxLogger.logInfo("All Blocks Registered Successfully!");
		}
	}
	
	public Block isLightEmitter(){
		this.isLightEmitter = true;
		return this;
	}
	
	public Block isMirror(){
		this.isMirror = true;
		return this;
	}
	
	public Block isGeneral(){
		this.isGeneral = true;
		return this;
	}
	
	public Block isAir(){
		this.isAir = true;
		return this;
	}
	
	public Block isLightReceiver(){
		this.isLightReceiver = true;
		return this;
	}
	
	public Block setTextureName(String textureName){
		this.textureName = textureName;
		textureArray[this.blockID] = textureName;
		return this;
	}
	
}
