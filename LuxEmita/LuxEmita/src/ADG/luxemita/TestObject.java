package ADG.luxemita;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class TestObject implements KeyListener {
	
	@Override
    public void keyTyped(KeyEvent e) {

    	if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            System.out.println("Right key pressed");
        }
        
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            System.out.println("Left key pressed");
        }

    }

    @Override
    public void keyPressed(KeyEvent e) {

    	if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            System.out.println("Right key pressed");
            
        	int left = 0+1;
        	int right = 0-1;
        	int up = 0;
        	int down = 0;
        	
        	String str = left-- + "|" + right++ + "|" + up + "|" + down;
        	
            System.out.println(str);
        }
        
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            System.out.println("Left key pressed");
            
        	int left = 0;
        	int right = 0;
        	int up = 0;
        	int down = 0;
        	
        	String str = left++ + "|" + right-- + "|" + up + "|" + down;
        	
            System.out.println(str);
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            System.out.println("Right key Released");
        }
        
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {       	
        	
        }
    }

}