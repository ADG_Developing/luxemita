package ADG.luxemita;


import org.lwjgl.opengl.Display;
import static org.lwjgl.opengl.GL11.*;
import ADG.luxemita.blocks.Block;
import ADG.luxemita.rendering.RenderingEngine;
import ADG.luxemita.util.osTweeker;

public class LuxEmita implements Runnable{

	public static LogAgent luxLogger; 
	public static String OSRunning; 
	
	public LuxEmita(){		
	    luxLogger = new LogAgent("[LuxEmita]", true, 64);		
	}
	
	public void gameLaunch()
	{
		
	}
	
	public void loadGame()
	{
		
		luxLogger.logInfo("Starting LuxEmita");
		luxLogger.logInfo("LuxEmita (C) ADG Development 2013 . All Rights Reserved.");

		osTweeker.getOS();
		
		luxLogger.logInfo("Running on: " + OSRunning);
		luxLogger.logInfo("Making rendering engine instance.");
		
		RenderingEngine.init();
		
		if(! RenderingEngine.renderingEngine.setUpLWJGL())
		{
			luxLogger.logInfo("Start Up Failed");
			return;
		}
		//DO NOT MOVE
		RenderingEngine.renderingEngine.createDisplay();
		RenderingEngine.renderingEngine.setUpOpenGL();
		
		Block.initBlocks();
		

		luxLogger.logInfo("Started up Successfully!");
	}
	

	@Override
	public void run() {
		
		loadGame();
		
		while(!Display.isCloseRequested())
		{
			glClear(GL_COLOR_BUFFER_BIT|GL_COLOR_BUFFER_BIT);
			CommandHandler.popCommandCue();
			
			glPushMatrix();
			{
				//PUT RENDER CODE IN HERE!
				
			}
			glPopMatrix();
			
			Display.update();
			Display.sync(60);
		}
		
		
		

	}
	
}
