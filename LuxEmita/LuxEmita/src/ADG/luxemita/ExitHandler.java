package ADG.luxemita;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class ExitHandler extends JPanel {

	private static final long serialVersionUID = 1L;
	public boolean exit;
	
	public ExitHandler(boolean exit){
		if(exit){
			this.exit = true;
		}
	}
	
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        this.setBackground(Color.BLACK);
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        
        if(this.exit){
        	System.exit(0);
        }
    }
}
