package ADG.luxemita;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import test.TextAreaOutputStream;

public class Start extends JFrame{
	
	private static final long serialVersionUID = 1L;
	//private static JPanel panel1 = new GuiSinglePlayer();
	private static JPanel panel2 = new GuiDevConsole();
	private static JPanel panel3 = new ExitHandler(true);
    private static Start frame = new Start();
    
    private static JTextArea textArea = new JTextArea(15, 30);
    private static TextAreaOutputStream taOutputStream = new TextAreaOutputStream(textArea, " Client", false);
    private static TextAreaOutputStream taOutputStream_err = new TextAreaOutputStream(textArea, " Client", true);
    public static LuxEmita luxemita;
    public static Thread luxGameThread;
    
	public Start(){
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    initMenu();
	    setLayout(new BorderLayout());
	}

	private class MenuAction implements ActionListener {
	    private JPanel panel;
	    
	    private MenuAction(JPanel pnl) {
	        this.panel = pnl;
	    }
	    
	    @Override
	    public void actionPerformed(ActionEvent e) {		    
	        changePanel(panel);
	    }
	}

	private void initMenu() {
	    JMenuBar menubar = new JMenuBar();
	    JMenu menu = new JMenu("Start");
	    JMenuItem sp = new JMenuItem("Singleplayer");
	    JMenuItem devconsole = new JMenuItem("Development Console");
	    JMenuItem exit = new JMenuItem("Exit");
	    
	    menubar.add(menu);
	    
	    menu.add(sp);
	    menu.add(devconsole);
	    menu.add(exit);
	    
	    setJMenuBar(menubar);
	    
	    sp.addActionListener(new GuiSinglePlayer());
	    devconsole.addActionListener(new MenuAction(panel2));
	    exit.addActionListener(new MenuAction(panel3));
	}

	private void changePanel(JPanel panel) {		
	    getContentPane().removeAll();
	    getContentPane().add(panel, BorderLayout.CENTER);
	    getContentPane().doLayout();
	    update(getGraphics());
	}

	public static void main(String[] args) throws Exception {
        frame.setPreferredSize(new Dimension(800, 600));
        frame.setSize(new Dimension(800, 600));
        frame.setMinimumSize(new Dimension(200, 100));
        frame.setLocation(250, 100);
        
        @SuppressWarnings("serial")
		final JPanel panel = new JPanel(){
            @Override
            public Dimension getPreferredSize(){
                return new Dimension(600, 500);
            }
        };
        
        @SuppressWarnings("serial")
		JScrollPane consoleOUT = new JScrollPane(textArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER){
            @Override
            public Dimension getPreferredSize(){
                return new Dimension(frame.getWidth() - 100, frame.getHeight() - 200);
            }
        };
        
        panel.add(consoleOUT);
        
        System.setOut(new PrintStream(taOutputStream));
        System.setErr(new PrintStream(taOutputStream_err));
        
        @SuppressWarnings("serial")
		final JPanel buttonP = new JPanel(){
            @Override
            public Dimension getPreferredSize(){
                return new Dimension(200, 100);
            }
        };
        
        final JTextField msg = new JTextField(15);
        
        @SuppressWarnings("serial")
		final JButton button = new JButton("Send"){
            @Override
            public Dimension getPreferredSize(){
                return new Dimension(100, 20);
            }
        };
        
        buttonP.setLayout(new FlowLayout(FlowLayout.CENTER));
        
        button.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent event){
        		send(msg.getText());
        	}
        });
        
        buttonP.add(msg);
        buttonP.add(button);
        
        frame.add(panel);
        frame.add(buttonP, BorderLayout.SOUTH);
	    frame.setVisible(true);
	    frame.setTitle("LuxEmita");
	    
	    luxemita = new LuxEmita();
	    luxGameThread = new Thread(luxemita);
	    luxGameThread.setName("Game Thread");
	}
	
	private static void send(String cmd){
		try {
			CommandHandler.send(cmd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
