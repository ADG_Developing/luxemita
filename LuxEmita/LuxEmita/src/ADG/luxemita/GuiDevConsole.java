package ADG.luxemita;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class GuiDevConsole extends JPanel{

	private static final long serialVersionUID = 1L;
	
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        this.setBackground(Color.BLACK);
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    }
        
}
