package entity;

public class EntityLaser {

	public int X;
	public int Y;
	public int[] direction;
	
	public void setDirection(int x, int y){
		this.direction[x] = x;
		this.direction[y] = y;
	}
	
	public int getEndPosX(){
		return this.X;
	}
	
	public int getEndPosY(){
		return this.Y;
	}
	
}
