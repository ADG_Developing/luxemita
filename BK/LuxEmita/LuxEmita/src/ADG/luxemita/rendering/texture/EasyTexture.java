package ADG.luxemita.rendering.texture;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.opengl.GL11;

import ADG.luxemita.LuxEmita;
import ADG.luxemita.rendering.RenderingEngine;

/** This is a some what simpler texture engine. Use this for basic texture drawing and loading.**/
public class EasyTexture {
	/** A map that is for URL to bounding texture conversion**/
	Map<String , Integer> textures;
	
	public EasyTexture()
	{
		textures = new HashMap<String , Integer>();
		
		LuxEmita.luxLogger.logInfo("Easy Texture Engine Instance Made.");
				
	}
	
	public int getBindingIntForURL(String URL)
	{
		if (textures.containsKey(URL))
		{
			return textures.get(URL);
		}
		else
		{
			LuxEmita.luxLogger.logInfo(URL + " has not been binded to any interger. Doing it now asuming that the texture is using GL_LINEAR");
			textures.put(URL, RenderingEngine.textureEngine.loadTexture(RenderingEngine.textureEngine.loadImage(URL), GL11.GL_LINEAR));
			return textures.get(URL);
		}
	}
}
