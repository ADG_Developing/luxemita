package ADG.luxemita.rendering.texture;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL12;

import ADG.luxemita.LuxEmita;
import static org.lwjgl.opengl.GL11.*;
/** It is suggested that you do not use the Texture Engine directy due to the fact it requires allot of OpenGl stuff. **/
public class TextureEngine {
	
	public TextureEngine()
	{
		LuxEmita.luxLogger.logInfo("Texture Engine Instance Made.");
	}
	/**
	 * @param URL - The URL of the image
	 * @return BufferImage
	 */
	public BufferedImage loadImage(String URL) {
        try {
            return ImageIO.read(new File(URL));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
	
 /**
 * @param image - The buffer image (Obtained via loadIamge(String URL)) of the image you want to turn into a texture.
 * @param glblur - Tells OpenGL how to scale images. Use GL_NEAREST for pixulated and GL_LINEAR for blured.
 * @return
 */
 public int loadTexture(BufferedImage image , int glblur){
      
      int[] pixels = new int[image.getWidth() * image.getHeight()];
        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());

        ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 4); //4 for RGBA, 3 for RGB
        
        for(int y = 0; y < image.getHeight(); y++){
            for(int x = 0; x < image.getWidth(); x++){
                int pixel = pixels[y * image.getWidth() + x];
                buffer.put((byte) ((pixel >> 16) & 0xFF));     // Red component
                buffer.put((byte) ((pixel >> 8) & 0xFF));      // Green component
                buffer.put((byte) (pixel & 0xFF));               // Blue component
                buffer.put((byte) ((pixel >> 24) & 0xFF));    // Alpha component. Only for RGBA
            }
        }

        buffer.flip(); //FOR THE LOVE OF GOD DO NOT FORGET THIS

        // You now have a ByteBuffer filled with the color data of each pixel.
        // Now just create a texture ID and bind it. Then you can load it using 
        // whatever OpenGL method you want, for example:

      int textureID = glGenTextures(); //Generate texture ID
        glBindTexture(GL_TEXTURE_2D, textureID); //Bind texture ID
        
        //Setup wrap mode
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);

        //Setup texture scaling filtering
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, glblur);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, glblur);
        
        //Send texel data to OpenGL
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, image.getWidth(), image.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
      
        //Return the texture ID so we can bind it later again
      return textureID;
   }
 /**
 * @param ID - The bound integer of the texture.
 * @param x
 * @param y
 * @param topRightX
 * @param topLeftX
 * @param bottomLeftX
 * @param bottomRightX
 * @param topRightY
 * @param topLeftY
 * @param bottomLeftY
 * @param bottomRightY
 * A rather complex way of drawing a texture. DO NOT USE UNLESS YOU KNOW WHAT IT ALL MEANS!
 */
public void drawTextureComplex(int ID , double x , double y, double topRightX , double topLeftX , double bottomLeftX , double bottomRightX, double topRightY , double topLeftY , double bottomLeftY , double bottomRightY)
 {

        glPushMatrix();
         glTranslated(x,y, 0.0);
 glBindTexture(GL_TEXTURE_2D, ID);
 glBegin(GL_QUADS);
 {
         glTexCoord2f(1, 1);
         glVertex2d(topRightX, topRightY);
    
         glTexCoord2f(0, 1);
    glVertex2d(topLeftX, topLeftY);
    
    glTexCoord2f(0, 0);
    glVertex2d(bottomLeftX,bottomLeftY);
    
    glTexCoord2f(1, 0);
    glVertex2d(bottomRightX, bottomRightY);
    glPopMatrix();
    
    
 }
 glEnd();
 
 }
 public void drawTextureSimple(int ID ,double x ,double y ,double sizeX ,double sizeY)
 {
         
         drawTextureComplex(ID , x, y , sizeX , 0 ,0,sizeX,0,0,sizeY,sizeY);
 }


}
