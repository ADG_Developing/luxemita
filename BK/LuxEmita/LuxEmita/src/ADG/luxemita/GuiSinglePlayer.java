package ADG.luxemita;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import ADG.luxemita.rendering.RenderingEngine;

public class GuiSinglePlayer extends JPanel implements ActionListener{

	private static final long serialVersionUID = 1L;
	
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        this.setBackground(Color.BLACK);
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.addKeyListener(new TestObject());
        
        Graphics2D g2 = (Graphics2D)g;

        Font f = new Font("Arial", 20, 20);  
        
        String msg = "Singleplayer, Work Under Progress!";  // The text to measure and display
        
        g2.setFont(f);
        g2.drawString(msg, (this.getWidth()/2) - (msg.length() * 4.3F), this.getHeight()/2);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		RenderingEngine.renderingEngine.createDisplay();
	}
}
