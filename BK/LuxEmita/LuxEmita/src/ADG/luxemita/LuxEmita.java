package ADG.luxemita;

import ADG.luxemita.blocks.Block;
import ADG.luxemita.rendering.RenderingEngine;
import ADG.luxemita.util.osTweeker;

public class LuxEmita {

	public static LogAgent luxLogger; 
	public static String OSRunning; 
	
	public LuxEmita(){		
	    luxLogger = new LogAgent("[LuxEmita]", true, 64);		
	}
	
	public void loadGame(){
		luxLogger.logInfo("Starting LuxEmita");
		luxLogger.logInfo("LuxEmita (C) ADG Development 2013 . All Rights Reserved.");
		
		//System Info
		osTweeker.getOS();
		luxLogger.logInfo("Running on: " + OSRunning);
		
		//LWJGL - LWJGL
		luxLogger.logInfo("Making rendering engine instance.");
		RenderingEngine.init();
		
		if(! RenderingEngine.renderingEngine.setUpLWJGL())
		{
			luxLogger.logInfo("Start Up Failed");
			return;
		}
		
		luxLogger.logInfo("Creating the all mighty display (Lets your computer can handle it).");
		
		Block.initBlocks();		
		
		luxLogger.logInfo("Started up Successfully!");
		
	}
	
}
