package logic;

import entity.EntityLaser;

public class LaserLogic {

	public static void bend(boolean bend){				
		EntityLaser laser = new EntityLaser();
		
		if(bend){
			laser.setDirection(laser.getEndPosX() + 4, laser.getEndPosY() - 4);
		}
	}
	
}
