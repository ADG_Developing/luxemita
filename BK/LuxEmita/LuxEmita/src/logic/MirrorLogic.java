package logic;

public class MirrorLogic {

	public boolean isLighted = false;
	
	public MirrorLogic(boolean bool){
		this.isLighted = bool;
	}
	
	public void lightIn(){
		if(this.isLighted){
			LaserLogic.bend(true);
		}
	}
	
}
